import * as types from './types'
import * as mutation_types from './mutation_types'
import { Get_dogs, Get_breeds } from './api/methods'

export default {
	[types.A_GET_DOGS](context) {
		return new Promise((resolve) => {
			Get_dogs()
				.then(res => {
					context.commit(mutation_types.M_GET_DOGS, res)
					resolve(res.data.message)
				})
		})
	},
	[types.A_GET_DOGS_BREED](context,payload) {
		return new Promise((resolve) => {
			Get_dogs(payload)
				.then(res => {
					context.commit(mutation_types.M_GET_DOGS_BREEDS, res)
					resolve(res.data.message)
				})
		})
	},
	[types.A_GET_BREEDS](context) {
		Get_breeds()
			.then(res => {
				let breeds = []
				for (let [key] of Object.entries(res.data.message)) {
					const item = {
						tabSelected: false,
						name: key,
						id: key
					}
					breeds.push(item)
				}
				context.commit(mutation_types.M_GET_BREEDS, breeds)
			})
	},
	[types.A_SAVE_DOGS_FAVORITE](context, payload) {
		context.commit(mutation_types.M_SAVE_DOGS_FAVORITE, payload)
	}
}