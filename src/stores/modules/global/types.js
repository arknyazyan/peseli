export const A_GET_DOGS = 'A_GET_DOGS'
export const A_GET_BREEDS = 'A_GET_BREEDS'
export const A_SAVE_DOGS_FAVORITE = 'A_SAVE_DOGS_FAVORITE'
export const A_GET_DOGS_BREED = 'A_GET_DOGS_BREED'