import Vue from 'vue'
export default () => {
	return Vue.Api.Get('https://dog.ceo/api/breeds/list/all')
}