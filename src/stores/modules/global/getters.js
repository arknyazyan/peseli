export default {
	getDogs: (state) => {
		return state.dogs
	},
	getBreeds: (state) => {
		return state.breeds
	},
	getFavoritesDogs: (state) => {
		return state.favorites
	}
}
