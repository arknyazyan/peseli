import Vue from 'vue'
import Vuex from 'vuex'
import state from './state'
import actions from './actions'
import mutations from './mutations'
import getters from './getters'
import Global from './modules/global/store'

Vue.use(Vuex)

export default new Vuex.Store({
	state,
	getters,
	mutations,
	actions,
	modules: {
		global: Global,
	},
})