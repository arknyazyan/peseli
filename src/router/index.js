import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/pages/main.vue'
import Favorite from '@/pages/favorites'

Vue.use(Router)

export default new Router({
	routes: [
		{
			path: '/',
			name: 'Main',
			component: Main
		},
		{
			path: '/favorites',
			name: 'Favorite',
			component: Favorite
		}
	]
})
